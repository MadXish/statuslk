'use strict';

App.factory('ATCService', ['$http','$q','UrlService',function($http,$q,UrlService){
	return {
		getStatus : function(oName, nicNo){
			return $http.get(UrlService.baseUrl+'api/open/'+oName+'/'+nicNo)
			.then(
					function(response){
						return response.data;
					}, 
					function(errResponse){
						console.error('Error while gettin status');
						return $q.reject(errResponse);
					}
			);
		}
	};
}]);