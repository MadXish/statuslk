'use strict';

App.controller('ATCController',['$scope', 'ATCService','$log', function($scope, ATCService,$log){
	self = this;
	
	self.fName;
	self.lName;
	self.age;
	self.gender ="";
	self.oName = "Duo Software";
	self.nicNo;
	self.alertShow = false;
	self.alertMsg = "";
	self.fSubmit = false;
	self.fSubmits = false;
	self.nicNoV = false;
	self.nicNoI = false;
	self.submitBtn = true;
	self.homeBtn = false;
	
	
	self.CustomerSearch = function(query){
		return CustomerService.getCustomerNameLike(query);
	};
	
	self.submit = function(){
		$log.info("run get call with oName - "+self.oName+" nicNo - "+self.nicNo);
		var res = ATCService.getStatus(self.oName,self.nicNo);
		res.then(
				function(data){
					$log.info(data);
					resProc(data);
				},
				function(error){
					$log.info(error);
					errResProc(error.data);
				}
		);
	}
	
	function resProc(data){
		self.fSubmits = true;
		self.nicNoI = false;
		if(data.sClass = "CRITICAL_STATUS"){
			self.alertMsg = "Your status level is not enough to apply for this job";
			self.alertShow = true;
			self.nicNoI = false;
			self.nicNoV = true;
		}else if(data.sClass = "BAD_STATUS"){
			self.alertMsg = "Your have to done exam before apply for this job";
			self.alertShow = true;
			self.nicNoI = false;
			self.nicNoV = true;
		}
		else{
			self.fSubmit = true;
			self.nicNoV = true;
			self.submitBtn = false;
			self.homeBtn = true;
			self.alertShow = false;
		}
	}
	
	function errResProc(error){
		self.fSubmits = true;
		if(error.statusCode == 404){
			self.alertMsg = "Invalid NIC No";
			self.nicNoI = true;
		}else{
			self.alertMsg = "Some error occured while submiting application, Please try again";
		}
		self.alertShow = true;
	}
	
	
}]);