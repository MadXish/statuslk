<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Application Submit Form</title>

	<link href=" <c:url value="/resources/css/demo.css"/> " rel="stylesheet" />
	<link href=" <c:url value="/resources/css/form-mini.css"/> " rel="stylesheet" />
    <link href=" <c:url value="/resources/css/bootstrap.css"/> " rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    
	<script src="<c:url value="/resources/js/angular.min.js"/>"></script> 
		 
	<script src="<c:url value="/resources/src/app.js"/>"></script>
	<script src="<c:url value="/resources/src/controller/atc_controller.js"/>"></script>
	<script src="<c:url value="/resources/src/service/url_service.js"/>"></script>
	<script src="<c:url value="/resources/src/service/atc_service.js"/>"></script>
</head>
<body style="background-color:#f2f2f2" ng-app="app" ng-controller="ATCController as ctrl">
   
    <div class="main-content">

        <!-- You only need this form and the form-mini.css -->

        <div class="form-mini-container">

<!--             <h1 style="padding-top:10px;">Basic Information</h1> -->

			<div ng-show="ctrl.fSubmit" class="form-row">
				<div class="alert alert-success" role="alert">Your Application submit successfully</div>
			</div>
			
<!--             <form class="form-mini" method="post" action="#" style="margin-left:-130px; width:600px"> -->

<!--                 <div class="form-row"> -->
<!--                     <b style="font-size:120%">First Name:</b> -->
<!--                     <input type="text" ng-disabled="ctrl.fSubmit" ng-model="ctrl.fName" name="lastName" placeholder="Enter first name here" style="width:390px; margin-left:55px;"> -->
<!--                     <i ng-show="ctrl.fSubmits" class="fa fa-check" style="color:green"></i> -->
<!--                 </div> -->

<!--                 <div class="form-row"> -->
<!--                     <b style="font-size:120%">Last Name:</b> -->
<!--                     <input ng-disabled="ctrl.fSubmit" type="text" name="lastName" ng-model="ctrl.lName" placeholder="Enter last name here" style="width:390px; margin-left:55px;"> -->
<!--                 	<i ng-show="ctrl.fSubmits" class="fa fa-check" style="color:green"></i> -->
<!--                 </div> -->
                
<!--                 <div class="form-row"> -->
<!--                     <b style="font-size:120%">Age :</b> -->
<!--                     <input ng-disabled="ctrl.fSubmit" type="text" ng-model="ctrl.age" name="age" placeholder="Enter your age" style="width:390px; margin-left:105px;"> -->
<!--                 	<i ng-show="ctrl.fSubmits" class="fa fa-check" style="color:green"></i> -->
<!--                 </div> -->
                
<!--                 <div class="form-row"> -->
<!--                     <b style="font-size:120%">Gender:</b> -->
<!--                     <label style="margin-left:10px;"> -->
<!--                         <select ng-disabled="ctrl.fSubmit" ng-model="ctrl.gender" name="dropdowngender" style="margin-left:74px;"> -->
<!--                             <option value="">Choose an option</option> -->
<!--                             <option value="1">Male</option> -->
<!--                             <option value="2">Female</option> -->
<!--                         </select> -->
<!--                     </label> -->
<!--                     <i ng-show="ctrl.fSubmits" class="fa fa-check" style="color:green"></i> -->
<!--                 </div> -->
<!--             </form> -->
        </div>
        
        <div class="form-mini-container">
            <h1 style="padding-top:10px; margin-top:10px;">Organizational Status</h1>
            <form class="form-mini" method="post" action="#" style="margin-left:-130px; width:600px;">
            <div ng-show="ctrl.alertShow" class="form-row">
            	<div class="alert alert-danger" role="alert">{{ctrl.alertMsg}}</div>
            </div>
                 <div class="form-row">
                        <b style="font-size:120%">Organization:</b>
                        <label style="margin-left:10px;">
                        <select ng-disabled="ctrl.fSubmit" ng-model="ctrl.oName" name="organization" style=" width:320px;margin-left:10px;">
                            <option value="DuoSoft">Duo Software</option>
                            <option value="UbiSoft">UbiSoft</option>
                        </select>
                    </label>
                 </div>

<!--                   <div class="form-row"> -->
<!--                         <b style="font-size:120%">Degree:</b> -->
<!--                         <label style="margin-left:10px;"> -->
<!--                         <select ng-disabled="ctrl.fSubmit" ng-model="ctrl.dType" name="degreeType" style=" width:320px;margin-left:120px;"> -->
<!--                             <option value="SOFTWARE_ENGINEERING">Software Engineering</option> -->
<!--                         </select> -->
<!--                     </label> -->
<!--                  </div> -->
                 
                 <div class="form-row">
                        <b style="font-size:120%">NIC No:</b>
                        <input ng-disabled="ctrl.fSubmit" type="text" ng-model="ctrl.nicNo" name="regno" placeholder="Enter NIC number" style="width:320px; margin-left:70px;">
                 		<i ng-show="ctrl.nicNoV" class="fa fa-check" style="color:green"></i>
                 		<i ng-show="ctrl.nicNoI" class="fa fa-times" aria-hidden="true" style="color:red"></i>
                 </div> 
                 <div class="form-row form-last-row">
                        <button ng-show="ctrl.submitBtn"  type="button" ng-click="ctrl.submit()" style="margin-left:370px;">Submit</button>
                        <button ng-show="ctrl.homeBtn" onclick="window.location='http://localhost:8080/APIConsumer/atc/application';" type="button" style="margin-left:370px;">Home</button>
                 </div>
                
            </form>
       </div>

    </div>

</body>

</html>
