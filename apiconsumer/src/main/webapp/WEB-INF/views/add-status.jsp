<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Add New</title>

	<link href=" <c:url value="/resources/css/demo.css"/> " rel="stylesheet" />
	<link href=" <c:url value="/resources/css/form-mini.css"/> " rel="stylesheet" />
    <link href=" <c:url value="/resources/css/bootstrap.css"/> " rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body  style="background-color:#f2f2f2">
    <ul style="height:90px">
    <b style="float: left; font-size: 250%;">Add Status</b>
        <li style="margin-left:900px;"><a href="<c:url value="/nsse/view"/>">Home</a></li>
        <li><a href="<c:url value="/nsse/add"/>" class="default">Add New</a></li>
    </ul>


    <div class="main-content">

        <!-- You only need this form and the form-mini.css -->

        <div class="form-mini-container">

			<c:if test="${err != '' }">
				<div class="alert alert-danger" role="alert">${err }</div>
			</c:if>
			
<!--             <h1>Add Status</h1> -->

            <form class="form-mini" method="post" action="#" style="width:1700px; margin-left:-600px; background-color: gray;">

                <div class="form-row" style="margin-left: 430px;">
                    <b style="font-size:120%; color: white;">NIC No:</b>
                    <input type="text" name="nicNo" placeholder="NIC Number" style="width:390px; margin-left:80px;">  
                </div>
                
                <div class="form-row" style="margin-left: 430px;">
                    <b style="font-size:120%; color: white;">Status:</b>
                    <label>
                        <select name="sClass" style="margin-left:86px;width:390px;">
                            <option value="CRITICAL_STATUS">Critical</option>
                            <option value="BAD_STATUS">Bad</option>
                            <option value="GOOD_STATUS">Good</option>
                            <option value="EXCELLENT_STATUS">Excellent</option>
                        </select>
                    </label>
                </div>
                
                <div class="form-row" style="margin-left: 435px;">
                    <b style="font-size:120%; color: white;">Points:</b>
                    <input type="text" name="points" placeholder="Enter Points" style="width:390px; margin-left:80px;">  
                </div>
				
                <div class="form-row form-last-row" style="margin-left: 635px;">
                    <button type="submit">Add Status</button>
                    <button onclick="window.location='http://www.example.com';" >Cancel</button>
                </div>

            </form>
        </div>


    </div>

</body>

</html>
