<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Add New</title>

	<link href=" <c:url value="/resources/css/demo.css"/> " rel="stylesheet" />
	<link href=" <c:url value="/resources/css/form-mini.css"/> " rel="stylesheet" />
    <link href=" <c:url value="/resources/css/bootstrap.css"/> " rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body  style="background-color:#f2f2f2">
    <ul style="height:90px">
    	<b style="float: left; font-size: 250%;">Update Status</b>
        <li style="margin-left:800px;"><a href="index.html">Home</a></li>
        <li><a href="form-mini.html" class="default">Add New</a></li>
    </ul>


    <div class="main-content">

        <!-- You only need this form and the form-mini.css -->

        <div class="form-mini-container">


<!--             <h1>Update Result</h1> -->

            <form class="form-mini" method="post" action="<c:url value="/nsse/edit/${status.nicNo}"/>" style="width:1700px; margin-left:-600px; background-color: gray;">

                <div class="form-row" style="margin-left: 435px;">
                    <b style="font-size:120%; color: white;">NIC No:</b>
                    <input type="text" name="nicNo" value="${status.nicNo }" placeholder="NIC Number" style="width:390px; margin-left:47px;">  
                </div>

<!--                 <div class="form-row"> -->
<!--                     <b style="font-size:120%">Degree Type:</b> -->
<!--                     <label> -->
<!--                         <select name="type" style="margin-left:32px;width:390px;"> -->
<%--                             <option value="INFORMATION_TECHNOLOGY" <c:if test="${result.type.toString() == 'INFORMATION_TECHNOLOGY'}">selected</c:if>>MATION TECHNOLOGY</option> --%>
<%--                             <option value="MANAGMENT" <c:if test="${result.type.toString() == 'MANAGMENT'}">selected</c:if>>MANAGMENT</option> --%>
<%--                             <option value="SCIENCE" <c:if test="${result.type.toString() == 'SCIENCE'}">selected</c:if>>SCIENCE</option> --%>
<%--                             <option value="ENGINEERING" <c:if test="${result.type.toString() == 'ENGINEERING'}">selected</c:if>>ENGINEERING</option> --%>
<%--                             <option value="SOFTWARE_ENGINEERING" <c:if test="${result.type.toString() == 'SOFTWARE_ENGINEERING'}">selected</c:if>>SOFTWARE ENGINEERING</option> --%>
<!--                         </select> -->
<!--                     </label> -->
<!--                 </div> -->
                
                <div class="form-row" style="margin-left: 435px;">
                    <b style="font-size:120%; color: white;">Status:</b>
                    <label>
                        <select name="sClass" style="margin-left:55px;">
                            <option value="CRITICAL_STATUS"<c:if test="${status.sClass.toString() == 'CRITICAL_STATUS'}">selected</c:if>>Critical Status</option>
                            <option value="BAD_STATUS"<c:if test="${status.sClass.toString() == 'BAD_STATUS'}">selected</c:if>>Bad Status</option>
                            <option value="GOOD_STATUS"<c:if test="${status.sClass.toString() == 'GOOD_STATUS'}">selected</c:if>>Good Status</option>
                            <option value="EXCELLENT_STATUS"<c:if test="${status.sClass.toString() == 'EXCELLENT_STATUS'}">selected</c:if>>Excellent Status</option>
                        </select>
                    </label>
                </div>
                
                <div class="form-row" style="margin-left: 435px;">
                    <b style="font-size:120%; color: white;">Points:</b>
                    <input type="text" name="points" value="${status.points}" placeholder="Enter Points" style="width:390px; margin-left:55px;">  
                </div>

                <div class="form-row form-last-row"  style="margin-left: 635px;">
                    <button type="submit" class="btn btn-default">Update</button>
<%--                     <a href="<c:url value="/nsse/view"/>" class="btn btn-default">Cancel</a> --%>
                </div>

            </form>
        </div>
    </div>

</body>

</html>
