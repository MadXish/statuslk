<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Home</title>
	<link href=" <c:url value="/resources/css/demo.css"/> " rel="stylesheet" />
	<link href=" <c:url value="/resources/css/form-basic.css"/> " rel="stylesheet" />
	<link href=" <c:url value="/resources/css/form-search.css"/> " rel="stylesheet" />
    <link href=" <c:url value="/resources/css/bootstrap.css"/> " rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

</head>
    
 <body  style="background-color:#f2f2f2">
    <ul>
    	<b style="float: left; font-size: 250%;">All Status</b>
        <li style="margin-left:900px;"><a href="<c:url value="/nsse/view"/>" class="defult">Home</a></li>
        <li><a href="<c:url value="/nsse/add"/>">Add New</a></li>
    </ul>


    <div class="main-content" style="width:100%; background-color: gray; padding-bottom:10px;">

        <form class="form-search" method="post" action="<c:url value="/nsse/search"/>">
            <input type="text" name="nic" placeholder="NIC No" style="margin-top:10px;">
            <button type="submit">Search</button>
            <i class="fa fa-search"></i>
        </form>
        <c:if test="${err != ''}">
        	<div class="alert alert-danger" style="text-align:center;" role="alert">${err}</div>    
        </c:if>
        <form class="form-basic" method="post" action="#" style="margin-top:20px;">
            
            <table class="table">
    <thead>
        <tr>
            <th style="text-align: center;">NIC No</th>
<!--             <th style="text-align: center;">Type</th> -->
            <th style="text-align: center;">Status</th>
            <th style="text-align: center;">Points</th>
            <th style="text-align: center;">Action</th>
        </tr>
    </thead>
    <tbody>
	    <c:forEach items="${statuses}" var="status">
	    	<tr class="info">
	            <td>${status.nicNo}</td>
<%-- 	            <td>${status.type}</td> --%>
	            <td>${status.sClass}</td>
	            <td>${status.points}</td>
	            <td><a href="<c:url value="/nsse/delete/${status.nicNo}"/>"><i class="fa fa-trash"></i></a> | <a href="<c:url value="/nsse/edit/${status.nicNo}"/>">Edit</a></td>
        	</tr>
	    </c:forEach>
    </tbody>
</table>

        </form>

    </div>

</body>

</html>
