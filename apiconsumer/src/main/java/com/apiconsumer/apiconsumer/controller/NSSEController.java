package com.apiconsumer.apiconsumer.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.apiconsumer.apiconsumer.model.Status;

@Controller
@RequestMapping("/nsse")
public class NSSEController {

	@Value("${TOKEN}")
	private String token;
	
	@RequestMapping("/view")
	public ModelAndView index(){
		ModelAndView model = new ModelAndView("view-status");
		RestTemplate t = new RestTemplate(); 
		String url = "http://localhost:8080/statusLK/api/secure/"+token;
		List<Status> statuses = null;
		String resErr = "";
		try{
			ResponseEntity<List<Status>> rateResponse =
			        t.exchange(url,HttpMethod.GET, null, new ParameterizedTypeReference<List<Status>>(){});
			statuses = rateResponse.getBody();
			System.out.println("status size -"+statuses.get(0).getNicNo());
		}catch(HttpStatusCodeException e){
			if(e.getStatusCode() == HttpStatus.NOT_FOUND){
				resErr = "Status Not found";
				System.out.println();
			}else{
				resErr = "error occured while getting status";
			}
		} catch (RestClientException ex) {
			ex.printStackTrace();
		    // Some other error occured, not related to an actual status code
		    // return whatever you need to in this case, maybe another 5xx code.
		}
		model.addObject("err", resErr);
		model.addObject("statuses", statuses);
		return model;
	}
	
	@RequestMapping(value="/add", method = RequestMethod.GET)
	public ModelAndView showAddPage(){
		ModelAndView model = new ModelAndView("add-status");
		model.addObject("err", "");
		return model;
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String add(@ModelAttribute("status") Status status){
		RestTemplate t = new RestTemplate(); 
		String url = "http://localhost:8080/statusLK/api/secure/"+token;
		String resErr = "";
		Status sStatus = null;
		try{
		    RestTemplate restTemplate = new RestTemplate();
		    restTemplate.postForObject(url, status, Status.class);
		}catch(HttpStatusCodeException e){
			if(e.getStatusCode() == HttpStatus.EXPECTATION_FAILED){
				resErr = "'"+status.getNicNo()+"' couldn't save due to invalid data";
			}else{
				resErr = "error occured while saving status";
			}
		} catch (RestClientException ex) {
			ex.printStackTrace();
		    // Some other error occured, not related to an actual status code
		    // return whatever you need to in this case, maybe another 5xx code.
		}
		return "redirect:/nsse/view";
	}
	
	@RequestMapping(value = "/delete/{nicNo}", method = RequestMethod.GET)
	public String delete(@PathVariable("nicNo") String nicNo){
		RestTemplate t = new RestTemplate(); 
		String url = "http://localhost:8080/statusLK/api/secure/"+nicNo+"/"+token;
		Status dStatus = null;
		String resErr = "";
		
		try{
			ResponseEntity<Status> rateResponse =
			        t.exchange(url,HttpMethod.DELETE, null, new ParameterizedTypeReference<Status>(){});
			dStatus = rateResponse.getBody();
			System.out.println("user name -"+dStatus.getNicNo());
		}catch(HttpStatusCodeException e){
			if(e.getStatusCode() == HttpStatus.NOT_FOUND){
				resErr = "'"+nicNo+"' not exist";
			}else{
				resErr = "error occured while deleting status";
			}
			
		} catch (RestClientException ex) {
			ex.printStackTrace();
		    // Some other error occured, not related to an actual status code
		    // return whatever you need to in this case, maybe another 5xx code.
		}
		return "redirect:/nsse/view";
	}
	
	@RequestMapping(value = "/edit/{nicNo}", method = RequestMethod.POST)
	public ModelAndView edit(@ModelAttribute("status") Status status, @PathVariable("nicNo") String nicNo){
		ModelAndView model = new ModelAndView();
		RestTemplate t = new RestTemplate(); 
		String url = "http://localhost:8080/statusLK/api/secure/{nicNo}/"+token;
		String resErr = "";
		
		try{
			Map<String, String> params = new HashMap<String, String>();
		    params.put("nicNo", nicNo);
		    t.put ( url, status, params);
		    model.setViewName("redirect:/nsse/view");
		}catch(HttpStatusCodeException e){
			model.setViewName("add-status");
			model.addObject("err", resErr);
			if(e.getStatusCode() == HttpStatus.NOT_FOUND){
				resErr = "'"+nicNo+"' not exist";
			}else{
				resErr = "error occured while updating status";
			}
			
		} catch (RestClientException ex) {
			ex.printStackTrace();
		    // Some other error occured, not related to an actual status code
		    // return whatever you need to in this case, maybe another 5xx code.
		}
		return model;
	}
	
	@RequestMapping(value = "/edit/{nicNo}" , method = RequestMethod.GET)
	public ModelAndView editShowPage(@PathVariable("nicNo") String nicNo){
		ModelAndView model = new ModelAndView("edit-status");
		String url = "http://localhost:8080/statusLK/api/secure/"+nicNo+"/"+token;
		Status status = null;
		String err = "";
		RestTemplate restTemplate = new RestTemplate();
		try{
			status = restTemplate.getForObject(url, Status.class);
			System.out.println("status nic -"+status.getNicNo());
		}catch(HttpStatusCodeException e){
			if(e.getStatusCode() == HttpStatus.NOT_FOUND){
				err = "'"+nicNo+"' not exist";
			}else{
				err = "error occured while updating status";
			}
		} catch (RestClientException ex) {
			ex.printStackTrace();
		    // Some other error occurred, not related to an actual status code
		    // return whatever you need to in this case, maybe another 5xx code.
		}
		model.addObject("status", status);
		model.addObject("err", err);
		return model;
	}
	
	@RequestMapping(value = "/search" , method = RequestMethod.POST)
	public ModelAndView search(@RequestParam("nic") String nicNo){
		ModelAndView model = new ModelAndView("view-status");
		String url = "http://localhost:8080/statusLK/api/secure/"+nicNo+"/"+token;
		List<Status> statuses = new ArrayList<Status>();
		String err = "";
		RestTemplate restTemplate = new RestTemplate();
		try{
			Status status = restTemplate.getForObject(url, Status.class);
			statuses.add(status);
			System.out.println("status nic -"+status.getNicNo());
		}catch(HttpStatusCodeException e){
			if(e.getStatusCode() == HttpStatus.NOT_FOUND){
				err = "'"+nicNo+"' not exist";
			}else{
				err = "error occured while search status";
			}
		} catch (RestClientException ex) {
			ex.printStackTrace();
		    // Some other error occurred, not related to an actual status code
		    // return whatever you need to in this case, maybe another 5xx code.
		}
		model.addObject("statuses", statuses);
		model.addObject("err", err);
		return model;
	}
}
