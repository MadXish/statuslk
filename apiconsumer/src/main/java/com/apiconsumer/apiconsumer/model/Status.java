package com.apiconsumer.apiconsumer.model;

public class Status {

	private String nicNo;

	private StatusClass sClass;

	private double points;

	public String getNicNo() {
		return nicNo;
	}

	public void setNicNo(String nicNo) {
		this.nicNo = nicNo;
	}

	public StatusClass getsClass() {
		return sClass;
	}

	public void setsClass(StatusClass sClass) {
		this.sClass = sClass;
	}

	public double getPoints() {
		return points;
	}

	public void setPoints(double points) {
		this.points = points;
	}
}
