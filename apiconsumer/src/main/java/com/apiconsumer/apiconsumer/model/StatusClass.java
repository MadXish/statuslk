package com.apiconsumer.apiconsumer.model;

public enum StatusClass {
	
	CRITICAL_STATUS(110),
	BAD_STATUS(111),
	GOOD_STATUS(112),
	EXCELLENT_STATUS(113);
	
	private int code;
	
	private StatusClass(int code){
		this.code = code;
	}
	
	public Integer getCode(){
		return this.code;
	}
}
