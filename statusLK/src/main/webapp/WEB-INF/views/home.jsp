<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login and Register</title>

        <!-- CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="<c:url value="/resources/assets/bootstrap/css/bootstrap.min.css"/>">
        <link rel="stylesheet" href="<c:url value="/resources/assets/css/font-awesome.min.css"/>">
		<link rel="stylesheet" href="<c:url value="/resources/assets/css/form-elements.css"/>">
        <link rel="stylesheet" href="<c:url value="/resources/assets/css/style.css"/>">

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                
                <header style="float:right; margin-right:50px">
<!--                     Hi User! &nbsp&nbsp&nbsp&nbsp -->
<!--                     <a href="#">LogOut</a> -->
                </header>
                
                <div class="container">
                	
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1>Login or Register</h1>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-5">
                        	
                        	<div class="form-box">
	                        	<div class="form-top">
	                        		<div class="form-top-left">
	                        			<h3>Login</h3>
	                            		<p>Enter username and password to log on:</p>
	                        		</div>
	                        		<div class="form-top-right">
	                        			<i class="fa fa-key"></i>
	                        		</div>
	                            </div>
	                            <div class="form-bottom">
	                            <c:url var="loginUrl" value="/login" />
				                    <form role="form" action="${loginUrl}" method="post" class="login-form">
				                    
				                    <c:if test="${not empty error}">
				                    		<div class="alert alert-danger">
			                                    <p>${error}</p>
			                                </div>
										</c:if>
										<c:if test="${not empty msg}">
											<div class="alert alert-success">
			                                    <p>${msg}</p>
			                                </div>
										</c:if>
										
				                    	<div class="form-group">
				                    		<label class="sr-only" for="form-username">Organization</label>
				                        	<input type="text" name="username" placeholder="Organization.." class="form-username form-control" id="form-username">
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-password">Password</label>
				                        	<input type="password" name="password" placeholder="Password..." class="form-password form-control" id="form-password">
				                        </div>
				                        <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
				                        <button type="submit" class="btn">Sign in!</button>
				                    </form>
			                    </div>
		                    </div>
	                        
                        </div>
                        
                        <div class="col-sm-1 middle-border"></div>
                        <div class="col-sm-1"></div>
                        	
                        <div class="col-sm-5">
                        	
                        	<div class="form-box">
                        		<div class="form-top">
	                        		<div class="form-top-left">
	                        			<h3>Sign up now</h3>
	                            		<p>Fill in the form below to get instant access:</p>
	                        		</div>
	                        		<div class="form-top-right">
	                        			<i class="fa fa-pencil"></i>
	                        		</div>
	                            </div>
	                            <div class="form-bottom">
				                    <form role="form" action="<c:url value="/home"/>" method="post" class="registration-form">
				                    	<div class="form-group">
				                    		<label class="sr-only" for="form-first-name">Organization</label>
				                        	<input type="text" value="${organization.name}" name="name" placeholder="Name..." class="form-first-name form-control" id="form-first-name">
				                        	<span class="text-danger"><form:errors path="organization.name" /></span>
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-last-name">Password</label>
				                        	<input type="password" value="${organization.pass}" name="pass" placeholder="Password..." class="form-last-name form-control" id="form-last-name">
				                        </div>
				                        <span class="text-danger"><form:errors path="organization.pass" /></span>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-email">Confirm</label>
				                        	<input type="password" name="confirm" placeholder="Confirm..." class="form-email form-control" id="form-email">
				                        </div>
				                        <span class="text-danger"><form:errors path="organization.confirm"/></span>
				                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				                        <button type="submit" class="btn">Sign me up!</button>
				                    </form>
			                    </div>
                        	</div>
                        	
                        </div>
                    </div>
                    
                </div>
                
                <div class="row">
                
                <div class="col-md-6 col-md-offset-3">
                <h3>Open API Documentation</h3>
                <img src="<c:url value="/resources/images/var.png"/>" class="img-responsive"> 
	            <img src="<c:url value="/resources/images/Open.png"/>" class="img-responsive"> 
	            </div>
                </div> 
                <div class="row">
                <h4>No need of token to use Open API</h4>
                </div>
                <div class="row">
                <h3>View StatusLK Documentation</h3>
	            <a href="<c:url value="/resources/images/StatusLK Documantation.docx"/>">Click here to download documantation</a>
                </div> 
            </div>
            
        </div>

        <!-- Footer -->
        <footer>
        	<div class="container">
        	</div>
        </footer>

        <!-- Javascript -->
        <script src="<c:url value="/resourcesassets/bootstrap/js/bootstrap.min.js"/>"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>