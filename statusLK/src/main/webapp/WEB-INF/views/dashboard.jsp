<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Get token</title>

        <!-- CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="<c:url value="/resources/assets/bootstrap/css/bootstrap.min.css"/>">
        <link rel="stylesheet" href="<c:url value="/resources/assets/css/font-awesome.min.css"/>">
		<link rel="stylesheet" href="<c:url value="/resources/assets/css/form-elements.css"/>">
        <link rel="stylesheet" href="<c:url value="/resources/assets/css/style.css"/>">

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                
                <header style="float:right; margin-right:50px">
<%--                    <c:url value="/logout" var="logoutUrl" /> --%>
<%--                    <form action="${logoutUrl}" method="post" id="logoutForm"> --%>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    Hi User! &nbsp&nbsp&nbsp&nbsp
                    
                    <c:url value="/logout" var="logoutUrl">
                    <c:param name="yourParamName" value="${logoutUrl}" />
                    </c:url>
                    <a href="${logoutUrl}">LogOut</a>
                    
<%--                    </form> --%>
                </header>
                
                <div class="container">
                	
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1>Get Your Own Token</h1>
                        </div>
                    </div>
                    
                    <div class="row">
                        <!--<div class="col-sm-1 middle-border" style="margin-top:5px;"></div>
                        <div class="col-sm-1"></div>-->
                        	
                        <div class="col-sm-8 col-sm-offset-2">
                        	
                        	<div class="form-box">
                        		<!--<div class="form-top">
	                        		<div class="form-top-left">
	                        			<h3>Sign up now</h3>
	                            		<p>Fill in the form below to get instant access:</p>
	                        		</div>
	                        		<div class="form-top-right">
	                        			<i class="fa fa-pencil"></i>
	                        		</div>
	                            </div>-->
	                            <div class="form-bottom">
				                    <form role="form" action="<c:url value="/dashboard"/>" method="post" class="registration-form">
<!-- 				                    	<div class="form-group"> -->
<!-- 				                    		<label class="sr-only" for="form-first-name">Token</label> -->
<!-- 				                        	<input type="text" name="form-first-name" placeholder="Token" disabled class="form-first-name form-control" id="Token"> -->
<!-- 				                        </div> -->
<!-- 				                        <button type="submit" class="btn">Genarate Token</button> -->
				                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				                        <c:choose>
				                        	<c:when test="${org.token != null }">
				                        		<h3 style="text-align:center;">${org.token}</h3>
				                        	</c:when>
				                        	<c:otherwise>
				                        		<h3 style="color:#fff; text-align:center;">Generate your authentication token</h3>
				                        		<button type="submit" class="btn">Generate</button>
				                        	</c:otherwise>
				                        </c:choose>
				                    </form>
			                    </div>
                        	</div>
                        	
                        </div>
                    
                    <div class="row">
                    <div class="col-md-6">
                      <div class="col-sm-8 col-sm-offset-2">
                        	
                        	<div class="form-box">
	                        	<div class="form-top">
	                        		<div class="form-top-left">
	                        			<h3>Authantication Token</h3>
	                            		<p>Genarate unique tocken to tour web site</p>
	                        		</div>
	                        		<div class="form-top-right">
	                        			<i class="fa fa-key"></i>
	                        		</div>
	                            </div>
	                            <div class="form-bottom">
                                    <div class="form-group">
				                    		<label class="" for="form-username">Organization Name &nbsp;&nbsp;&nbsp;:</label>
                                            <spane class="max" for="form-username">${org.name}</spane>
				                        	
				                        </div>
                                        
                                        <div class="form-group">
				                        	<label class="" for="form-password">Release Status &nbsp;&nbsp;&nbsp;&nbsp;:</label>
				                        	<spane class="max" for="form-username"><strong>${count}</strong></spane>
				                        </div>
                                        
<!--                                         <div class="form-group"> -->
<!-- 				                        	<label class="" for="form-password">Status View &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</label> -->
<%-- 				                        	<spane class="max" for="form-username"><strong>${org.viewCount}</strong></spane> --%>
<!-- 				                        </div> -->
                                        
                                        <div class="form-group">
				                        	<label class="" for="form-password">Register Date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</label>
				                        	<spane class="max" for="form-username">
				                        	${org.registerDate}
				                        	</spane>
				                        </div>
			                    </div>
		                    </div>
	                        
                        </div>
                        </div> 
                        
                        <div class="col-md-6">
                                   <div class="row">
<!--                         <div class="col-sm-8 col-sm-offset-2 text"> -->
                            <h1 style="margin-top:30px;">Devoloping Information</h1>
<!--                         </div> -->
           </div>
          
          <div class="row">
<!--               <div class="col-md-6 col-md-offset-3"> -->
<!--                   <div class="col-md-12 col-xs-12 "> -->
                      <div class="form">
	                        	<div class="form-top">
	                        		<div class="form-top">
	                        		
	                        		<h3 class="col-md-offset-3" style="margin-left: 100px;">Download StatusLK Documentation</h3>
	                        		<a class="col-md-offset-3" href="<c:url value="/resources/images/StatusLK Documantation.docx"/>">Click here to download documantation</a>
	                        		
	                        		<div class="row">
	                        		<h3 class="col-md-offset-3">Secure API Documentation</h3>
	                        		</div>
	                        		<div class="row">
	                        		<img src="<c:url value="/resources/images/var.png"/>" class="img-responsive"> 
	                        		<img src="<c:url value="/resources/images/get getall.png"/>" class="img-responsive"> 
	                        		</div>
	                        		<div class="row">
	                        		<img src="<c:url value="/resources/images/POst PUt.png"/>" class="img-responsive"> 
	                        		</div>
	                        		<div class="row">
	                        		<img src="<c:url value="/resources/images/Delete.png"/>" class="img-responsive"> 
	                        		</div>
	                        		</div>
	                        		
	                        		<div class="row">
	                        		<h3 class="col-md-offset-3">Why Authantication Token Needs?</h3>
	                        		Authanication tokens are used to prove identification electronically.It is good for transmitting information securely.
	                        		Because as they can be signed, for example using public/private key pairs,
	                        		you can be sure that the senders are who they say they are. Additionally, as the signature is calculated using the header and the payload, you can also verify that the content hasn't been tampered with. 
	                        		<br>
	                        		<br>
	                        		<h3 class="col-md-offset-3">How to get Token</h3>
	                        		To use secure API for your organization, you must have to get your own token. token key can use for your consumer app/apps in oraganization. 
	                        		<br>
	                        		1. First You have to register with PAI sevice using StatusLK website.<br>
	                        		2. After login to account, you can own token key for your organization.
	                        		<br>
	                        		<br>
	                        		For complete documentation, Download documantataion using link on the top of this section
	                        		</div>
	                        		
	                            </div>
		                    </div>
	                        
<!--                         </div> -->
<!--                   </div> -->
              </div>
                        </div> 
                    </div>
                    
                    
                    
                    </div>
                    
                </div>
            </div>
           

          </div>

        <!-- Footer -->
        <footer>
        	<div class="container">
        	</div>
        </footer>

        <!-- Javascript -->
        <script src="<c:url value="/resources/assets/bootstrap/js/bootstrap.min.js"/>"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>