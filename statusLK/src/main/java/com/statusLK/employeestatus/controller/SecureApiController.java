package com.statusLK.employeestatus.controller;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.statusLK.employeestatus.model.Status;
import com.statusLK.employeestatus.model.Organization;
import com.statusLK.employeestatus.service.StatusService;
import com.statusLK.employeestatus.service.OrganizationService;

@RestController
@RequestMapping("/api/secure")
public class SecureApiController {

	@Autowired
	private OrganizationService organizationService;
	
	@Autowired
	private StatusService statusService;
	
	
	//GET - get all result of particular university
	@RequestMapping(value="/{token}", produces = MediaType.APPLICATION_JSON_VALUE, method=RequestMethod.GET)
	public ResponseEntity<Object> getResults(@PathVariable("token") String token){
		Organization org = organizationService.getOrganizationByToken(token);
		if(org != null){
			Collection<Status> status = statusService.getStatus(org);
			if(!status.isEmpty()){
				return new ResponseEntity<Object>(status,HttpStatus.OK);
			}
			return new ResponseEntity<Object>("Status not found",HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Object>("Authentication fail",HttpStatus.UNAUTHORIZED);
	}
	
	//POST - save result by JSON object
	@RequestMapping(value="/{token}", produces = MediaType.APPLICATION_JSON_VALUE, method=RequestMethod.POST)
	public ResponseEntity<Object> saveResults(@Valid @RequestBody Status status, BindingResult bindErrors ,@PathVariable("token") String token){
		Organization org = organizationService.getOrganizationByToken(token);
		if(org != null){
			if(bindErrors.hasErrors()){
				return new ResponseEntity<Object>(bindErrors.getFieldError().getDefaultMessage(),HttpStatus.CONFLICT);
			}
			status.setOrganization(org);
			Status sStatus = statusService.saveStatus(status);		
			if(sStatus != null){
				return new ResponseEntity<Object>(sStatus,HttpStatus.OK);
			}
			return new ResponseEntity<Object>(status,HttpStatus.EXPECTATION_FAILED);
		}
		return new ResponseEntity<Object>("Authentication fail",HttpStatus.UNAUTHORIZED);
	}
	
	//GET - get a result by index number
	@RequestMapping(value="/{nicNo}/{token}", produces = MediaType.APPLICATION_JSON_VALUE, method=RequestMethod.GET)
	public ResponseEntity<Object> getResult(@PathVariable("nicNo") String nicNo, @PathVariable("token") String token){
		Organization org = organizationService.getOrganizationByToken(token);
		if(org != null){
			Status status = statusService.getStatus(nicNo);
			if(status != null){
				return new ResponseEntity<Object>(status,HttpStatus.OK);
			}
			return new ResponseEntity<Object>("Status not found",HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Object>("Authentication fail",HttpStatus.UNAUTHORIZED);
	}
	
	//PUT - update result by JSON object
	@RequestMapping(value="/{nicNo}/{token}", produces = MediaType.APPLICATION_JSON_VALUE, method=RequestMethod.PUT)
	public ResponseEntity<Object> updateResult(@Valid @RequestBody Status rStatus, BindingResult bindErrors , @PathVariable("token") String token,@PathVariable("nicNo") String nicNo){
		Organization org = organizationService.getOrganizationByToken(token);
		if(org != null){
			if(bindErrors.hasErrors()){
				return new ResponseEntity<Object>(bindErrors.getFieldError().getDefaultMessage(),HttpStatus.CONFLICT);
			}
			Status status = statusService.getStatus(nicNo);
			System.out.println("status id - "+status.getId());
			if(status != null){
				rStatus.setOrganization(org);
				Status oResult = statusService.updateStatus(status.getId(), rStatus);
				if(oResult != null){
					return new ResponseEntity<Object>(oResult,HttpStatus.OK);
				}
				return new ResponseEntity<Object>(rStatus,HttpStatus.EXPECTATION_FAILED);	
			}
			return new ResponseEntity<Object>(rStatus,HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Object>("Authentication fail",HttpStatus.UNAUTHORIZED);
	}
	
	//DELETE - delete result by id
	@RequestMapping(value="/{nicNo}/{token}", produces = MediaType.APPLICATION_JSON_VALUE, method=RequestMethod.DELETE)
	public ResponseEntity<Object> deleteResult(@PathVariable("token") String token , @PathVariable("nicNo") String nicNo){
		Organization org = organizationService.getOrganizationByToken(token);
		if(org != null){
			Status status = statusService.getStatus(nicNo);
			if(status != null){
				Status dStatus = statusService.deleteStatus(status);
				if(dStatus != null){
					return new ResponseEntity<Object>(status,HttpStatus.OK);
				}
				return new ResponseEntity<Object>(status,HttpStatus.EXPECTATION_FAILED);
			}
			return new ResponseEntity<Object>("Status not found",HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Object>("Authentication fail",HttpStatus.UNAUTHORIZED);
	}
	
}
