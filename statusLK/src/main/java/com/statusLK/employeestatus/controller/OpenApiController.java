package com.statusLK.employeestatus.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.statusLK.employeestatus.model.ResponceError;
import com.statusLK.employeestatus.model.Status;
import com.statusLK.employeestatus.service.StatusService;

@RestController
@RequestMapping("/api/open")
public class OpenApiController {

	@Autowired
	private StatusService statusService;
	
	@RequestMapping(value="/{oName}/{nicNo}", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getResult(@PathVariable("oName") String orgName, @PathVariable("nicNo") String nicNo){
		
		Status result = statusService.getStatus(orgName, nicNo);
		if(result != null){
			System.out.println("found");
			return new ResponseEntity<Object>(result,HttpStatus.OK);
		}else{
			System.out.println("not found");
			return new ResponseEntity<Object>(new ResponceError(HttpStatus.NOT_FOUND.value(),"Status Not Found"),HttpStatus.NOT_FOUND);
		}
	}
}
