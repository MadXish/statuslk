package com.statusLK.employeestatus.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.statusLK.employeestatus.model.Status;
import com.statusLK.employeestatus.model.StatusClass;
import com.statusLK.employeestatus.model.StatusType;
import com.statusLK.employeestatus.model.Organization;
import com.statusLK.employeestatus.service.StatusService;
import com.statusLK.employeestatus.service.SecurityService;
import com.statusLK.employeestatus.service.TokenGeneratorService;
import com.statusLK.employeestatus.service.TokenGeneratorServiceImpl;
import com.statusLK.employeestatus.service.OrganizationService;

@Controller
public class WebController {

	@Autowired 
	SecurityService securityService;
	
	@Autowired
	OrganizationService organizationService;
	
	@Autowired
	StatusService statusService;
	
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout, HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		
		if (error != null) {
			model.addObject("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("home");
		return model;
	}
	
	// customize the error message
	private String getErrorMessage(HttpServletRequest request, String key) {

		Exception exception = (Exception) request.getSession().getAttribute(key);

		String error = "";
		if (exception instanceof BadCredentialsException) {
			error = "Invalid organization name or password!";
		} else if (exception instanceof LockedException) {
			error = exception.getMessage();
		} else {
			error = "Invalid organization name or password!";
		}
		return error;
	}
	
	@RequestMapping("/show-user")
	public ModelAndView user(){
		ModelAndView model = new ModelAndView();
		RestTemplate t = new RestTemplate(); 
		String url = "http://localhost:8080/resultpoint/api/user";
		Organization u = null;
		try{
			u = t.getForObject(url, Organization.class);
			System.out.println("user name -"+u.getName());
		}catch(HttpStatusCodeException e){
			int statusCode = e.getStatusCode().value();
			String res = e.getResponseBodyAsString();
			System.out.println("User obj -"+u);
			System.out.println("response obj as string -"+res);
			System.out.println("status code -"+statusCode);
		} catch (RestClientException ex) {
		    // Some other error occured, not related to an actual status code
		    // return whatever you need to in this case, maybe another 5xx code.
		}
		return model;
	}
	
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String showDashboardPage(ModelMap model) {
        model.addAttribute("user", securityService.findLoggedInUsername());
        System.out.println("loged user name -"+securityService.findLoggedInUsername());
        Organization org = organizationService.getOrganizationByName(securityService.findLoggedInUsername());
        model.addAttribute("org", org);
        model.addAttribute("count", statusService.getStatusCountOfOrganization(org.getId()));
        return "dashboard";
    }
	
	@RequestMapping(value = "/dashboard", method = RequestMethod.POST)
    public String generateToken(ModelMap model) {
        Organization org = organizationService.getOrganizationByName(securityService.findLoggedInUsername());
        organizationService.saveToken(org);
        return "redirect:/dashboard";
    }
    
    @RequestMapping(value = "/home", method = RequestMethod.POST)
    public ModelAndView register(@Valid @ModelAttribute("organization") Organization organization, 
    		BindingResult status) {
    	
    	ModelAndView model = new ModelAndView();
    	
    	if(!organization.getPass().equals(organization.getConfirm())){
    		status.rejectValue("confirm", "error.organization", "password is not match");
    	}
    	if(status.hasErrors()){
    		model.setViewName("home");
    		return model;
    	}
    	organizationService.saveOrganization(organization);
    	securityService.autologin(organization.getName(), organization.getPass());
    	model.setViewName("redirect:/dashboard");
    	return model;
    }
    
    @RequestMapping("/test")
	public String save(){
		//System.out.println("encode pass -"+passwordEncoder.encode("11111"));
		return "";
	}
	
//	System.out.println("reg get call");
//	securityService.autologin("Ishara", "11111");
//
//    return "redirect:/secure";
}
