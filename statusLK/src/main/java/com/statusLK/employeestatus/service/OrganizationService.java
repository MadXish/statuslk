package com.statusLK.employeestatus.service;

import com.statusLK.employeestatus.model.Organization;

public interface OrganizationService {

	public int saveOrganization(Organization organization);
	public Organization getOrganizationByName(String name);
	public void saveToken(Organization organization);
	public Organization getOrganizationByToken(String token);
}
