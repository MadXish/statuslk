package com.statusLK.employeestatus.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.statusLK.employeestatus.dao.OrganizationDao;
import com.statusLK.employeestatus.model.Organization;


@Service("organizationService")
@Transactional
public class OrganizationServiceImpl implements OrganizationService{

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private OrganizationDao organizationDao;
	
	@Autowired
	private TokenGeneratorService tokengeneratorService;
	
	@Override
	public int saveOrganization(Organization organization) {
		organization.setHashPass(passwordEncoder.encode(organization.getPass()));
		organization.setRegisterDate(new Date());
		return organizationDao.saveOrganization(organization); 
	}

	@Override
	public Organization getOrganizationByName(String name) {
		return organizationDao.getOrganizationByName(name);
	}

	@Override
	public void saveToken(Organization organization) {
		organization.setToken(tokengeneratorService.generateToken());
		organizationDao.updateOrganization(organization);
	}

	@Override
	public Organization getOrganizationByToken(String token) {
		return organizationDao.getOrganizationByToken(token);
	}

}
