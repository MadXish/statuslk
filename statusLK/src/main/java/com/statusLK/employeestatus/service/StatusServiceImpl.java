package com.statusLK.employeestatus.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.statusLK.employeestatus.dao.StatusDao;
import com.statusLK.employeestatus.model.Status;
import com.statusLK.employeestatus.model.Organization;

@Service("statusService")
public class StatusServiceImpl implements StatusService {

	@Autowired
	private StatusDao StatusDao;
	
	@Override
	public Status getStatus(String uName, String indexNo) {
		return StatusDao.getStatus(uName, indexNo);
	}

	@Override
	public Status saveStatus(Status result) {
		return StatusDao.saveStatus(result);
	}

	@Override
	public long getStatusCountOfOrganization(int id) {
		return StatusDao.getStatusCountOfOrganization(id);
	}

	@Override
	public Collection<Status> getStatus(Organization organization) {
		return StatusDao.getStatus(organization);
	}

	@Override
	public Status getStatus(String nicNo) {
		return StatusDao.getStatus(nicNo);
	}

	@Override
	public Status updateStatus(int id, Status status) {
		status.setId(id);
		return StatusDao.updateStatus(status);
	}

	@Override
	public Status deleteStatus(Status status) {
		return StatusDao.deleteStatus(status);
	}

}
