package com.statusLK.employeestatus.service;

public interface SecurityService {

	String findLoggedInUsername();
    void autologin(String username, String password);
}
