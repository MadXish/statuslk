package com.statusLK.employeestatus.service;

public interface TokenGeneratorService {

	public String generateToken();
}
