package com.statusLK.employeestatus.service;

import java.util.Collection;

import com.statusLK.employeestatus.model.Status;
import com.statusLK.employeestatus.model.Organization;

public interface StatusService {

	public Status getStatus(String uName,String nicNo);
	public Status saveStatus(Status status);
	public long getStatusCountOfOrganization(int id);
	public Collection<Status> getStatus(Organization organization);
	public Status getStatus(String nicNo);
	public Status updateStatus(int id,Status status);
	public Status deleteStatus(Status status);
}
