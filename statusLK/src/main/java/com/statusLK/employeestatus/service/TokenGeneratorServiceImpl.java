package com.statusLK.employeestatus.service;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

import org.springframework.stereotype.Service;

@Service("tokengeneratorService")
public class TokenGeneratorServiceImpl implements TokenGeneratorService {

	private SecureRandom sRand;
	
	public TokenGeneratorServiceImpl(){
		try {
			sRand = SecureRandom.getInstance("SHA1PRNG", "SUN");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String s = " ";
		sRand.nextBytes(s.getBytes(StandardCharsets.UTF_8));
	}
	
	public String generateToken(){
		sRand.setSeed(sRand.generateSeed(32));
		byte[] bytes = new byte[32];
		sRand.nextBytes(bytes);
		
		return encodeBytes(bytes);
	}
	
	private String encodeBytes( byte[] bytes ) {
		
		@SuppressWarnings("restriction")
		String token = new sun.misc.BASE64Encoder().encode(bytes);

		token = token.replaceAll("\\+", "-");
		token = token.replaceAll("/", "_");
		token = token.replaceAll("=", "");

		return token ;

	}
}











