package com.statusLK.employeestatus.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="status")
public class Status {

	private int id;
	
	@Size(min=4,max=15)
	private String nicNo;
		

	private StatusClass sClass;

	@Max(100)
	private double points = -1;
	
	@JsonIgnore
	private Organization organization;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name="nic_no", unique = true, nullable = false, length = 15)
	public String getnicNo() {
		return nicNo;
	}
	
	public void setnicNo(String nicNo) {
		this.nicNo = nicNo;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="status_class", nullable = false)
	public StatusClass getsClass() {
		return sClass;
	}

	public void setsClass(StatusClass sClass) {
		this.sClass = sClass;
	}
	
	@Column(name="points", precision=10, scale=2)
	public double getPoints() {
		return points;
	}
	public void setPoints(double points) {
		this.points = points;
	}
	
	@ManyToOne
	@JoinColumn(name="org_id")
	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	
}
