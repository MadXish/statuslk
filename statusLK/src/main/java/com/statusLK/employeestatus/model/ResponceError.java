package com.statusLK.employeestatus.model;

public class ResponceError {

	private int errorCode;
	private String msg;
	
	
	public ResponceError()
	{
		
	}
	
	public ResponceError(int errorCode, String msg)
	{
		this.errorCode = errorCode;
		this.msg = msg;
	}
	
	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
