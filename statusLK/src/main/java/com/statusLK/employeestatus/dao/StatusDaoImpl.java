package com.statusLK.employeestatus.dao;


import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.statusLK.employeestatus.model.Status;
import com.statusLK.employeestatus.model.Organization;


@Repository("statusDao")
public class StatusDaoImpl extends Dao implements StatusDao {

	@Override
	public Status getStatus(String oName, String nicNo) {

		Session session = this.getSessionFactory().openSession();
		Status status = null;
		try{
			Criteria criteria = session.createCriteria(Status.class)
					.createAlias("organization", "o")
					.add(Restrictions.eq("o.name", oName))
					.add(Restrictions.eq("nicNo", nicNo));
			
			status = (Status)criteria.uniqueResult();
		}catch (HibernateException e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return status;
	}

	@Override
	public Status saveStatus(Status status) {
		Session session = this.getSessionFactory().openSession();
		Transaction tx = null;
		Status sStatus = null;
		try{
			tx = session.beginTransaction();
			session.save(status);
			sStatus = status;
			session.getTransaction().commit();
		}catch(HibernateException e){
			if(tx != null){
				tx.rollback();
			}
			e.printStackTrace();
		}finally{
			session.close();
		}
		return sStatus;
	}

	@Override
	public long getStatusCountOfOrganization(int id) {
		Session session = this.getSessionFactory().openSession();
		long count = 0;
		try{
			Criteria criteria = session.createCriteria(Status.class)
					.createAlias("organization", "org")
					.add(Restrictions.eq("org.id", id))
					.setProjection(Projections.rowCount());
			count = (long)criteria.uniqueResult();
		}catch(HibernateException e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return count;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Status> getStatus(Organization organization) {
		Session session = this.getSessionFactory().openSession();
		Collection<Status> status = null;
		try{
			Criteria criteria = session.createCriteria(Status.class)
					.add(Restrictions.eq("organization", organization));
			status = (Collection<Status>)criteria.list();
		}catch(HibernateException e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return status;
	}

	@Override
	public Status getStatus(String nicNo) {
		Session session = this.getSessionFactory().openSession();
		Status status = null;
		try{
			Criteria criteria = session.createCriteria(Status.class)
					.add(Restrictions.eq("nicNo", nicNo));
			status = (Status)criteria.uniqueResult();
		}catch(HibernateException e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return status;
	}

	@Override
	public Status updateStatus(Status status) {
		Session session = this.getSessionFactory().openSession();
		Transaction tx = null;
		Status uStatus = null;
		try{
			tx = session.beginTransaction();
			session.update(status);
			session.getTransaction().commit();
			uStatus = status;
		}catch(HibernateException e){
			if(tx != null){
				tx.rollback();
			}
			e.printStackTrace();
		}finally{
			session.close();
		}
		return uStatus;
	}

	@Override
	public Status deleteStatus(Status status) {
		Session session = this.getSessionFactory().openSession();
		Transaction tx = null;
		Status dStatus = null;
		try{
			tx = session.beginTransaction();
			session.delete(status);
			session.getTransaction().commit();
			dStatus = status;
		}catch(HibernateException e){
			if(tx != null){
				tx.rollback();
			}
			e.printStackTrace();
		}finally{
			session.close();
		}
		return dStatus;
	}
}
