package com.statusLK.employeestatus.dao;

import com.statusLK.employeestatus.model.Organization;

public interface OrganizationDao {

	public int saveOrganization(Organization organization);
	public Organization getOrganizationByName(String name);
	public void updateOrganization(Organization organization);
	public Organization getOrganizationByToken(String token);
	
}
