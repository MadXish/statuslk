package com.statusLK.employeestatus.dao;

import java.util.Collection;

import com.statusLK.employeestatus.model.Status;
import com.statusLK.employeestatus.model.Organization;

public interface StatusDao {
	public Status getStatus(String oName,String nicNo);
	public Status saveStatus(Status status);
	public long getStatusCountOfOrganization(int id);
	public Collection<Status> getStatus(Organization organization);
	public Status getStatus(String nicNo);
	public Status updateStatus(Status status);
	public Status deleteStatus(Status tsatus);
}
