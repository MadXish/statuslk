package com.statusLK.employeestatus.dao;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.statusLK.employeestatus.model.Organization;

@Repository("organizationDao")
public class OrganizationDaoImpl extends Dao implements OrganizationDao {

	@Override
	public int saveOrganization(Organization organization) {
		Session session = this.getSessionFactory().openSession();
		Transaction tx = null;
		int id = 0;
		try{
			tx = session.beginTransaction();
			session.save(organization);
			id = organization.getId();
			session.getTransaction().commit();
		}catch(HibernateException e){
			if(tx != null){
				tx.rollback();
			}
			e.printStackTrace();
		}finally{
			session.close();
		}
		return id;
	}

	@Override
	public Organization getOrganizationByName(String name) {
		Session session = this.getSessionFactory().openSession();
		Organization organization = null;
		try{
			Criteria criteria = session.createCriteria(Organization.class)
					.add(Restrictions.eq("name", name));
			organization = (Organization)criteria.uniqueResult();
		}catch(HibernateException e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return organization;
	}

	@Override
	public void updateOrganization(Organization organization) {
		Session session = this.getSessionFactory().openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			session.update(organization);
			session.getTransaction().commit();
		}catch(HibernateException e){
			if(tx != null){
				tx.rollback();
			}
			e.printStackTrace();
		}finally{
			session.close();
		}
	}

	@Override
	public Organization getOrganizationByToken(String token) {
		Session session = this.getSessionFactory().openSession();
		Organization organization = null;
		try{
			Criteria criteria = session.createCriteria(Organization.class)
					.add(Restrictions.eq("token", token));
			organization = (Organization)criteria.uniqueResult();
		}catch(HibernateException e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return organization;
	}
}
